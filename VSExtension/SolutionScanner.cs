﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TmCompositeApplicationExtension.AttributeHandling;
using TmCompositeApplicationExtension.StructureInformation;

namespace TmCompositeApplicationExtension
{
    public static class SolutionScanner
    {
        /*public static async Task<StructureInfo> ScanSolution(string solutionFilename)
        {
            List<StructurePart> parts = new List<StructurePart>();
            StructureInfo structureInfo = new StructureInfo
            {
                CreationDateUtc = DateTime.Now,
                Parts = parts
            };

            //await ThreadHelper.JoinableTaskFactory.SwitchToMainThreadAsync();

            MSBuildWorkspace workspace = MSBuildWorkspace.Create();
            workspace.WorkspaceFailed += Workspace_WorkspaceFailed;
            Solution solution = await workspace.OpenSolutionAsync(solutionFilename);

            foreach (Microsoft.CodeAnalysis.Project project in solution.Projects)
            {
                foreach (var document in project.Documents)
                {
                    
                }
            }

            foreach (Microsoft.CodeAnalysis.Project project in solution.Projects)
            {
                ScanProject(project, parts);
            }

            return structureInfo;
        }*/

        public static async Task<StructureInfo> ScanSolution(Workspace workspace)
        {
            List<StructurePart> parts = new List<StructurePart>();
            StructureInfo structureInfo = new StructureInfo
            {
                CreationDateUtc = DateTime.Now,
                Parts = parts
            };

            Solution solution = workspace.CurrentSolution;
            

            foreach (Microsoft.CodeAnalysis.Project project in solution.Projects)
            {
                ScanProject(project, parts);
            }

            return structureInfo;
        }

        private static void Workspace_WorkspaceFailed(object sender, WorkspaceDiagnosticEventArgs e)
        {
            throw new Exception(e.Diagnostic.Message);
        }

        private static void ScanProject(Project project, List<StructurePart> parts)
        {
            Document[] docs = project.Documents.ToArray();
            Compilation compilation = project.GetCompilationAsync().Result;

            ScanNamespace(compilation.GlobalNamespace, parts);
        }

        private static void ScanNamespace(INamespaceSymbol @namespace, List<StructurePart> parts)
        {
            IEnumerable<INamespaceSymbol> namespaceMembers = @namespace.GetNamespaceMembers();

            foreach (INamespaceOrTypeSymbol member in @namespace.GetMembers())
            {
                StructurePart part = TryCreatePart(member);

                if (part != null)
                    parts.Add(part);
            }

            foreach (INamespaceSymbol namespaceMember in namespaceMembers)
            {
                System.Diagnostics.Debug.WriteLine("NAMESPACE: " + namespaceMember.Name);

                ScanNamespace(namespaceMember, parts);
            }
        }

        private static StructurePart TryCreatePart(INamespaceOrTypeSymbol symbol)
        {
            StructurePart structurePart = null;

            if (symbol.IsType)
            {
                List<StructurePartExport> exports = new List<StructurePartExport>();
                bool isExported = false;

                foreach (AttributeData attribute in symbol.GetAttributes())
                {
                    string contractName = ExportAttributeHandler.TryGetContractName(attribute, out bool hasExportAttribute);

                    if (hasExportAttribute)
                        isExported = true;

                    if (!string.IsNullOrWhiteSpace(contractName))
                        exports.Add(new StructurePartExport { ContractName = contractName });
                }

                string memberFullName = symbol.ContainingNamespace.ToDisplayString() + "." + symbol.Name;

                if (!exports.Any())
                {
                    string contractName = memberFullName;

                    exports.Add(new StructurePartExport { ContractName = contractName });
                }

                if (isExported)
                {
                    var filename = symbol.OriginalDefinition.Locations
                        .FirstOrDefault()?
                        .SourceTree?
                        .FilePath;

                    structurePart = new StructurePart
                    {
                        Name = symbol.Name,
                        FullName = memberFullName,
                        Exports = exports,
                        Comment = symbol.GetDocumentationCommentXml(),
                        Filename = filename
                    };

                    // Get the class import
                    List<StructurePartImport> imports = new List<StructurePartImport>();

                    foreach (ISymbol member in symbol.GetMembers())
                    {
                        StructurePartImport import = ImportAttributeHandler.TryGetImport(member);

                        if (import != null)
                            imports.Add(import);
                    }

                    structurePart.Imports = imports;

                    // Get path
                    /*if (symbol.Locations != null && symbol.Locations.Any())
                    {
                        if (symbol.Locations.Length > 1)
                            throw new NotImplementedException("Currently only one location per class is supported!");

                        // TODO: For now simply take the file location, but it should be location in the solution which can differ
                        structurePart.Path = symbol.Locations.Single().SourceTree.FilePath;
                    }*/
                }
            }

            return structurePart;
        }
    }
}
