﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TmCompositeApplicationExtension.StructureInformation;

namespace TmCompositeApplicationExtension.AttributeHandling
{
    public static class ImportAttributeHandler
    {
        public static StructurePartImport TryGetImport(ISymbol member)
        {
            StructurePartImport import = null;

            if (member.Kind == SymbolKind.Field)
            {
                string contactName = null;
                bool isImport = false;

                foreach (var attribute in member.GetAttributes())
                {
                    string attributeClassName = attribute.AttributeClass.ToDisplayString();

                    if (attributeClassName == "System.ComponentModel.Composition.ImportAttribute" ||
                        attributeClassName == "System.ComponentModel.Composition.ImportManyAttribute")
                    {
                        if (contactName != null)
                            throw new Exception("Field can have only one Import attribute");

                        // ImprotMany attribute has same constructor arguments as Import attribute
                        contactName = TryGetContractNameFromImportAttribute(attribute);
                        isImport = true;
                    }
                }

                if (isImport)
                {
                    // If not from attribute, get contract name from the field type
                    if (contactName == null)
                        contactName = ((IFieldSymbol)member).Type.ToDisplayString();

                    import = new StructurePartImport
                    {
                        ContractName = contactName,
                        Comment = member.GetDocumentationCommentXml()
                    };
                }
            }

            return import;
        }

        private static string TryGetContractNameFromImportAttribute(AttributeData attribute)
        {
            string contractName = null;

            if (attribute.ConstructorArguments.Length == 1)
            {
                TypedConstant typedConstant = attribute.ConstructorArguments[0];

                if (typedConstant.Type.Name == nameof(Type))
                    contractName = typedConstant.Value.ToString();
                else if (typedConstant.Type.Name == nameof(String))
                    contractName = typedConstant.Value.ToString();
                else
                    throw new NotImplementedException();
            }
            else if (attribute.ConstructorArguments.Length > 1)
                throw new NotImplementedException();

            return contractName;
        }
    }
}
