﻿using Microsoft.CodeAnalysis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmCompositeApplicationExtension.AttributeHandling
{
    public static class ExportAttributeHandler
    {
        public static string TryGetContractName(AttributeData attribute, out bool hasExportAttribute)
        {
            string contractName = null;
            hasExportAttribute = false;

            if (attribute.AttributeClass.ToDisplayString() == "System.ComponentModel.Composition.ExportAttribute")
            {
                // Get the contract from attribute parameter
                if (attribute.ConstructorArguments.Length == 1)
                {
                    TypedConstant typedConstant = attribute.ConstructorArguments[0];
                    hasExportAttribute = true;

                    if (typedConstant.Type.Name == nameof(Type))
                        contractName = typedConstant.Value.ToString();
                    else if (typedConstant.Type.Name == nameof(String))
                        contractName = typedConstant.Value.ToString();
                    else
                        throw new NotImplementedException();
                }
                else if (attribute.ConstructorArguments.Length > 1)
                    throw new NotImplementedException();
            }

            return contractName;
        }
    }
}
