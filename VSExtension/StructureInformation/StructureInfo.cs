﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TmCompositeApplicationExtension.StructureInformation
{
    public class StructureInfo
    {
        public DateTime CreationDateUtc { get; set; }
        public IEnumerable<StructurePart> Parts { get; set; }
    }

    /// <summary>
    /// Stores a data about a part (class or interface in code)
    /// </summary>
    public class StructurePart
    {
        public string Name { get; set; }
        public string FullName { get; set; }
        public IEnumerable<StructurePartExport> Exports { get; set; }
        public IEnumerable<StructurePartImport> Imports { get; set; }
        public string Comment { get; set; }
        public IEnumerable<StructurePartPropertyMember> Properties { get; set; }
        public IEnumerable<StructurePartMethodMember> Methods { get; set; }
        public IEnumerable<StructurePartEventMember> Events { get; set; }
        public string Filename { get; set; }
    }

    public class StructurePartPropertyMember
    {
        public string Name { get; set; }
        public string FullTypeName { get; set; }
        public bool IsReadonly { get; set; }
    }
    public class StructurePartMethodMember
    {
        public string Name { get; set; }
        public string ReturnFullTypeName { get; set; }
        public IEnumerable<StructurePartMethodMemberParameter> Parameters { get; set; }
    }
    public class StructurePartMethodMemberParameter
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
    }
    public class StructurePartEventMember
    {
        public string Name { get; set; }
        public string TypeName { get; set; }
    }

    public class StructurePartExport
    {
        public string ContractName { get; set; }
    }
    public class StructurePartImport
    {
        public string ContractName { get; set; }
        public string Comment { get; set; }
    }
    public class StructureModule
    {
        public string Name { get; set; }
        public string Path { get; set; }
        // TODO: description file contents
    }
    public class StructureComponent
    {
        public string Name { get; set; }
        // TODO: description file contents
    }
}
