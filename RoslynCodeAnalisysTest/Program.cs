﻿using Microsoft.Build.Locator;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.MSBuild;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoslynCodeAnalisysTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MSBuildLocator.RegisterDefaults();

            MSBuildWorkspace ws = MSBuildWorkspace.Create();
            ws.WorkspaceFailed += Ws_WorkspaceFailed;
            //ws.LoadMetadataForReferencedProjects = false;
            var solution = ws.OpenSolutionAsync(@"D:\Tomaz\Projects\ArchitectureVisualisation\RoslynCodeAnalisysTest\RoslynCodeAnalisysTest.sln").Result;

            Project project = solution.Projects.First();
            var compilation = project.GetCompilationAsync().Result;

            var classes = compilation.GlobalNamespace.GetNamespaceMembers().SelectMany(x => x.GetMembers()).ToArray();

            var parts = GetPartsFromNamespaceOrTypeSymbol(compilation.GlobalNamespace).ToArray();
        }

        private static void Ws_WorkspaceFailed(object sender, WorkspaceDiagnosticEventArgs e)
        {
            throw new Exception(e.Diagnostic.Message);
        }

        static IEnumerable<Part> GetPartsFromNamespaceOrTypeSymbol(ISymbol symbol)
        {
            var namespaceOrTypeSymbol = symbol as INamespaceOrTypeSymbol;

            if (namespaceOrTypeSymbol.IsType)
            {
                Part part = TryCreatePart(namespaceOrTypeSymbol);

                if (part != null)
                    yield return part;
            }
            else if (namespaceOrTypeSymbol.IsNamespace)
            {
                foreach (var item in namespaceOrTypeSymbol.GetMembers())
                {
                    var list = GetPartsFromNamespaceOrTypeSymbol(item);

                    foreach (var part in list)
                    {
                        yield return part;
                    }
                }
            }
        }

        static Part TryCreatePart(ISymbol symbol)
        {
            foreach (var attribute in symbol.GetAttributes())
            {
                INamedTypeSymbol attributeClass = attribute.AttributeClass;

                while (attributeClass != null)
                {
                    string typeName = attributeClass.ToString();

                    if (typeName == typeof(ExportAttribute).FullName)
                    {
                        return CreatePart(attribute);
                    }
                    // TODO: Add additional export types

                    attributeClass = attributeClass.BaseType;
                }
            }

            return null;
        }

        private static Part CreatePart(AttributeData exportAttribute)
        {
            //exportAttribute.
            return new Part();
        }
    }

    public class Part
    {
        public IEnumerable<Export> Exports { get; set; }
        public IEnumerable<Import> Imports { get; set; }
        public bool IsFloating { get; set; }
        public string LifetimeAttributeType { get; set; }

        // TODO: add member information
    }

    public class Export
    {
        public string ContractName { get; set; }
    }
    public class Import
    {
        public string ContractName { get; set; }
        public string FieldType { get; set; }
        public string Comment { get; set; }
        public ImportMethod ImportMethod { get; set; }
        public ImportCardinality ImportCardinality { get; set; }

        // TODO: somehow add information from aditional functionalities like
        // ReplacementExport from Testing, that doesn's derive from ExportAttribute
    }

    public enum ImportMethod { Direct, Lazy, ExportFactory }
    public enum ImportCardinality { ZeroOrOne, ExactlyOne, ZeroOrMore }
}
