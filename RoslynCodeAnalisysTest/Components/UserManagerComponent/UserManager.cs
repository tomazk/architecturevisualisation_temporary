﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoslynCodeAnalisysTest.Components.UserManagerComponent
{
    public interface IUserManager
    {
    }

    [Export(typeof(IUserManager))]
    public class UserManager
    {
        [Import]
        private IDatabase database = null;
    }

    public interface IDatabase
    {
    }
}
