﻿
namespace Graphviz4Net.Tests.Graphs
{
    using System.Linq;
    using NUnit.Framework;
    using Graphviz4Net.Graphs;

    [TestFixture]
    public class GraphRemoveVertexWithEdgesTests
    {
        private Graph<string> graph;

        [SetUp]
        public void SetUp()
        {
            this.graph = new Graph<string>();
            this.graph.AddVertex("a");
            this.graph.AddVertex("b");
            var subGraph = new SubGraph<string>();
            this.graph.AddSubGraph(subGraph);
            subGraph.AddVertex("c");

            this.graph.AddEdge(new Edge<string>("a", "b"));
            this.graph.AddEdge(new Edge<string>("b", "c"));
        }
    }
}
