﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Graphviz4Net.WPF.Example
{
    using Graphs;
    using Graphviz4Net.WPF.Example.Editor.LayoutBuilding;
    using Newtonsoft.Json;
    using System.ComponentModel;
    using System.Reflection;
    using System.Text;

    public class PartViewModel : INotifyPropertyChanged
    {
        private readonly Graph<PartViewModel> graph;
        private readonly StructurePart structurePart;

        public PartViewModel(Graph<PartViewModel> graph, StructurePart structurePart)
        {
            this.graph = graph;
            this.structurePart = structurePart;
        }

        public string Name => structurePart.Name;
        public string FullName => structurePart.FullName;

        public bool HasExport(string contractName)
        {
            return structurePart.Exports.Any(x => x.ContractName == contractName);
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class Arrow
    {        
    }

    public class MainWindowViewModel : INotifyPropertyChanged
    {
        public MainWindowViewModel()
        {
            var resourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Graphviz4Net.WPF.Example.SampleGraph.txt");
            byte[] jsonBytes = new byte[resourceStream.Length];
            resourceStream.Read(jsonBytes, 0, (int)resourceStream.Length);
            string json = Encoding.UTF8.GetString(jsonBytes);
            StructureInfo structureInfo = JsonConvert.DeserializeObject<StructureInfo>(json);

            // Create graph
            var graph = new Graph<PartViewModel>();
            List<PartViewModel> vertices = new List<PartViewModel>();

            GraphDescription graphDescription = new GraphDescription();

            foreach (var part in structureInfo.Parts)
            {
                // TODO: Prevent double entries. This is because of bug in the structure generator.
                if (vertices.Any(x => x.FullName == part.FullName))
                    continue;

                var vertex = new PartViewModel(graph, part);
                vertices.Add(vertex);
                graph.AddVertex(vertex);

                graphDescription.AddVertex(new VertexDescription { Width = 100, Height = 50, StructurePart = part });
            }
            
            foreach (var sourceVertex in graphDescription.Vertices)
            {
                foreach (var import in sourceVertex.StructurePart.Imports)
                {
                    var destinationVertices = graphDescription.Vertices.Where(x => x.StructurePart.HasExport(import.ContractName));

                    foreach (var destinationVertex in destinationVertices)
                    {
                        graphDescription.AddEdge(sourceVertex.Id, destinationVertex.Id);
                    }
                }
            }





            List<Tuple<PartViewModel, PartViewModel>> edges = new List<Tuple<PartViewModel, PartViewModel>>();

            foreach (var part in structureInfo.Parts)
            {
                foreach (var import in part.Imports)
                {
                    PartViewModel source = vertices.Single(x => x.FullName == part.FullName);
                    IEnumerable<PartViewModel> destinations = vertices
                        .Where(x => x.HasExport(import.ContractName));

                    if (destinations.Any())
                    {
                        // TODO: added because of the duplication bug
                        List<PartViewModel> addedVertices = new List<PartViewModel>();

                        foreach (var destination in destinations)
                        {
                            if (edges.Any(x => x.Item1 == source && x.Item2 == destination))
                                continue;

                            graph.AddEdge(new Edge<PartViewModel>(source, destination, new Arrow()));
                            addedVertices.Add(destination);
                            edges.Add(new Tuple<PartViewModel, PartViewModel>(source, destination));
                        }
                    }
                }
            }



            /*var subGraph = new SubGraph<Person> { Label = "Work" };
            graph.AddSubGraph(subGraph);
            subGraph.AddVertex(g);
            subGraph.AddVertex(h);
            graph.AddEdge(new Edge<Person>(g, h));
            graph.AddEdge(new Edge<Person>(a, g));

            var subGraph2 = new SubGraph<Person> {Label = "School"};
            graph.AddSubGraph(subGraph2);
            var loner = new Person(graph) { Name = "Loner" };
            subGraph2.AddVertex(loner);
            graph.AddEdge(new Edge<SubGraph<Person>>(subGraph, subGraph2) { Label = "Link between groups" } );*/

            this.Graph = graph;
            this.Graph.Changed += GraphChanged;
        }

        public Graph<PartViewModel> Graph { get; private set; }

        /*public void CreateEdge()
        {
            if (string.IsNullOrWhiteSpace(this.NewEdgeStart) ||
                string.IsNullOrWhiteSpace(this.NewEdgeEnd))
            {
                return;
            }

            this.Graph.AddEdge(
                new Edge<PartViewModel>
                    (this.GetPerson(this.NewEdgeStart), 
                    this.GetPerson(this.NewEdgeEnd))
                    {
                        Label = this.NewEdgeLabel
                    });
        }*/

        public event PropertyChangedEventHandler PropertyChanged;

        private void GraphChanged(object sender, GraphChangedArgs e)
        {
            
        }

        private void RaisePropertyChanged(string property)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
