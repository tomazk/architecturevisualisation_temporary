﻿
namespace Graphviz4Net.WPF.Example
{
    using Graphviz4Net.WPF.Example.Editor;
    using System.Windows;

    public partial class MainWindow : Window
    {
        private MainWindowViewModel viewModel;
        private EditorLayer mainLayer;

        public MainWindow()
        {
            this.viewModel = new MainWindowViewModel();
            this.DataContext = viewModel;
            InitializeComponent();

            mainLayer = editorView.AddLayer("Main");

            System.Windows.Controls.Button b = new System.Windows.Controls.Button();
            b.Content = "Click me";

            mainLayer.AddElement(b, new Point(100, 200));
        }
    }
}
