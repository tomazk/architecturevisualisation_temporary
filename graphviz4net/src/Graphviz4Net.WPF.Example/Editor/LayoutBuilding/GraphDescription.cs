﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphviz4Net.WPF.Example.Editor.LayoutBuilding
{
    public class GraphDescription
    {
        private List<VertexDescription> vertices = new List<VertexDescription>();
        private List<EdgeDescription> edges = new List<EdgeDescription>();
        private List<GroupDescription> groups = new List<GroupDescription>();

        public IEnumerable<VertexDescription> Vertices => vertices;
        public IEnumerable<EdgeDescription> Edges => edges;
        public IEnumerable<GroupDescription> Groups => groups;

        public void AddVertex(VertexDescription vertex)
        {
            vertices.Add(vertex);
        }
        public void AddEdge(int fromVertexId, int toVertexId)
        {
            edges.Add(new EdgeDescription { FromVertexId = fromVertexId, ToVertexId = toVertexId });
        }
        public void AddGroup(GroupDescription group)
        {
            groups.Add(group);
        }
    }

    public class VertexDescription
    {
        public int Id { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public StructurePart StructurePart { get; set; }
    }

    public class EdgeDescription
    {
        public int FromVertexId { get; set; }
        public int ToVertexId { get; set; }
    }

    public class GroupDescription
    {
        private List<VertexDescription> vertices = new List<VertexDescription>();
        private List<GroupDescription> groups = new List<GroupDescription>();

        public IEnumerable<VertexDescription> Vertices => vertices;
        public IEnumerable<GroupDescription> Groups => groups;

        public void AddVertex(VertexDescription vertex)
        {
            vertices.Add(vertex);
        }
        public void AddGroup(GroupDescription group)
        {
            groups.Add(group);
        }
    }

}
