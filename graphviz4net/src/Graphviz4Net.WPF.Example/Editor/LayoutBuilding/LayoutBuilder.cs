﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graphviz4Net.WPF.Example.Editor.LayoutBuilding
{
    public static class LayoutBuilder
    {
        public static void GetGraphLayout(GraphDescription graph)
        {
            string inputDot = ConvertGraphToDot(graph);
        }

        private static string ConvertGraphToDot(GraphDescription graph)
        {
            var nodesMap = new List<object>();
            var idsMap = new Dictionary<object, int>();

            StringBuilder sb = new StringBuilder();
            StringWriter originalWriter = new StringWriter(sb);
            var writer = new IndentedTextWriter(originalWriter);
            writer.WriteLine("digraph g { ");

            var graphAttributes = new Dictionary<string, string>();
            /*if (graph is IAttributed)
            {
                graphAttributes = new Dictionary<string, string>(((IAttributed)graph).Attributes);
            }*/

            // we don't need compound attribute if there are no sub-graphs
            if (graph.Groups.Any())
            {
                graphAttributes.Add("compound", "true");
            }

            writer.WriteLine("graph [{0}];", graphAttributes.GetGraphAttributes());

            writer.Indent++;
            foreach (var vertex in graph.Vertices)
            {
                writer.WriteLine("{0} [ {1} ];", nodesMap.Count, GetVertexAttributes(vertex));
                idsMap.Add(vertex, nodesMap.Count);
                nodesMap.Add(vertex);
            }

            /*var subGraphs = graph.Groups.ToArray();
            for (int i = 0; i < subGraphs.Length; i++)
            {
                var subGraph = subGraphs[i];
                writer.WriteLine("subgraph cluster{0} {{ ", nodesMap.Count);
                idsMap.Add(subGraph, nodesMap.Count);
                nodesMap.Add(subGraph);
                writer.Indent++;

                // In order to have clusters labels always on the top:
                // when the direction is from bottom to top, change label location to bottom, 
                // which in this direction means top of the image
                string rankdir;
                if (graph is IAttributed &&
                    ((IAttributed)graph).Attributes.TryGetValue("rankdir", out rankdir) &&
                    rankdir == RankDirection.BottomToTop)
                {
                    writer.WriteLine("graph [labelloc=b];");
                }

                if (subGraph is IAttributed)
                {
                    writer.WriteLine("graph [{0}];", ((IAttributed)subGraph).GetAttributes());
                }

                foreach (var vertex in subGraph.Vertices)
                {
                    writer.WriteLine("{0} [ {1} ];", nodesMap.Count, GetVertexAttributes(vertex, additionalAttributesProvider));
                    idsMap.Add(vertex, nodesMap.Count);
                    nodesMap.Add(vertex);
                }
                writer.Indent--;
                writer.WriteLine("}; ");
            }*/

            foreach (var edge in graph.Edges)
            {
                IDictionary<string, string> attributes = new Dictionary<string, string>();
                /*if (edge is IAttributed)
                {
                    attributes = ((IAttributed)edge).Attributes;
                }*/

                //attributes["dir"] = "both";

                /*object edgeSource = edge.Source;
                object edgeDestination = edge.Destination;

                if (edgeSource is ISubGraph)
                {
                    attributes["ltail"] = "cluster" + idsMap[edgeSource];
                    edgeSource = ((ISubGraph)edgeSource).Vertices.First();
                }

                if (edgeDestination is ISubGraph)
                {
                    attributes["lhead"] = "cluster" + idsMap[edgeDestination];
                    var subGraph = ((ISubGraph)edgeDestination);
                    edgeDestination = subGraph.Vertices.FirstOrDefault();
                    if (edgeDestination == null)
                    {
                        throw new InvalidOperationException(
                            "SubGraph must have at least one vertex. " +
                            string.Format("This does not hold for {0} subgraph. ", subGraph.Label) +
                            "Graphviz4Net cannot render empty subgraphs. ");
                    }
                }*/

                attributes["comment"] = nodesMap.Count.ToString();
                nodesMap.Add(edge);

                writer.WriteLine(
                    "{0}{1} -> {2}{3} [ {4} ];",
                    edge.FromVertexId,
                    string.Empty,
                    edge.ToVertexId,
                    string.Empty,
                    attributes.GetAttributes());
            }

            writer.Indent--;
            writer.WriteLine("}");   // end of the diagraph

            return sb.ToString();
        }

        public static string GetAttributes(this IDictionary<string, string> attributes)
        {
            if (attributes == null)
            {
                return string.Empty;
            }

            return string.Join(",", attributes.Select(GetAttributeAssignment));
        }

        public static string GetVertexAttributes(VertexDescription vertex)
        {
            Dictionary<string, string> attributes = new Dictionary<string, string>();
            attributes.Add("width", vertex.Width.ToInvariantString());
            attributes.Add("height", vertex.Height.ToInvariantString());
            attributes.Add("shape", "rect");
            attributes.Add("fixedsize", "true");

            return string.Join(",", attributes.Select(GetAttributeAssignment));
        }

        public static string GetGraphAttributes(this IDictionary<string, string> graphAttributes)
        {
            if (graphAttributes == null)
            {
                return string.Empty;
            }

            return string.Join(",", graphAttributes.Select(GetAttributeAssignment));
        }

        public static string GetAttributeAssignment(KeyValuePair<string, string> attribute)
        {
            // CW: This is to support HTML labels. The syntax is label=<<HTML... without quotes.
            if (attribute.Key.ToLower() == "label" &&
                attribute.Value.StartsWith("<<"))
            {
                return string.Format("{0}={1} ", attribute.Key, attribute.Value);
            }

            return string.Format("{0}=\"{1}\" ", attribute.Key, attribute.Value);
        }
    }
}
