﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Controls;

namespace Graphviz4Net.WPF.Example.Editor
{
    public class EditorBase : UserControl, INotifyPropertyChanged
    {
        protected void NotifyPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void NotifyPropertyChanged(Expression<Func<object>> expression)
        {
            if (PropertyChanged != null)
            {
                LambdaExpression lambda = expression as LambdaExpression;
                MemberExpression memberExpr = null;

                if (lambda.Body.NodeType == ExpressionType.Convert)
                {
                    memberExpr = ((UnaryExpression)lambda.Body).Operand as MemberExpression;
                }
                else if (lambda.Body.NodeType == ExpressionType.MemberAccess)
                {
                    memberExpr = lambda.Body as MemberExpression;
                }
                else
                    throw new NotImplementedException();

                PropertyChanged(this, new PropertyChangedEventArgs(memberExpr.Member.Name));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
