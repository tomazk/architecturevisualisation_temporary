﻿using Graphviz4Net.WPF.Example.Editor.UIActions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphviz4Net.WPF.Example.Editor
{
    public partial class EditorView : EditorBase
    {
        private LinkedList<UIActionBase> actionStack = new LinkedList<UIActionBase>();
        private PanAndZoomAction panAndZoomAction;

        private LinkedList<EditorLayer> layers = new LinkedList<EditorLayer>();
        private Transform transform = Transform.Identity;

        public Transform Tranform
        {
            get { return transform; }
            set
            {
                transform = value;

                backgroundGrid.Transform = transform;
                foreach (var layer in layers)
                {
                    layer.SetTransform(transform);
                }
            }
        }

        public EditorView()
        {
            InitializeComponent();

            MouseDown += Control_MouseDown;
            MouseMove += Control_MouseMove;
            MouseUp += Control_MouseUp;
            MouseWheel += Control_MouseWheel;

            panAndZoomAction = new PanAndZoomAction(this);
            AddUIAction(panAndZoomAction);
        }

        public IEnumerable<EditorLayer> Layers => layers;

        public EditorLayer AddLayer(string name)
        {
            var layer = new EditorLayer();
            layer.SetTransform(transform);
            // TODO: layer Z-index
            layers.AddLast(layer);
            uiLayersGrid.Children.Add(layer.Canvas);
            return layer;
        }

        // Workaround for key handling. MainForm will trigger this method because
        // control itself is not focusable and may not even be focused when performing
        // a certain action. The event has to therefore come from a higher up.
        public void HandleKeyDown(KeyEventArgs e)
        {
            foreach (var action in actionStack)
            {
                var result = action.HandleKeyDown(e);

                if (result == ActionResult.StopHere)
                    break;
            }
        }

        public void AddUIAction(UIActionBase action)
        {
            if (!actionStack.Contains(action))
            {

                actionStack.AddLast(action);
                action.Finished += Action_Finished;
                action.OnAdded();
            }
        }

        private void Control_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            foreach (var action in actionStack)
            {
                var result = action.HandleMouseWheel(e);

                if (result == ActionResult.StopHere)
                    break;
            }
        }

        private void Control_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (var action in actionStack)
            {
                var result = action.HandleMouseUp(e);

                if (result == ActionResult.StopHere)
                    break;
            }
        }

        private void Control_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            foreach (var action in actionStack)
            {
                var result = action.HandleMouseMove(e);

                if (result == ActionResult.StopHere)
                    break;
            }
        }

        private void Control_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            foreach (var action in actionStack)
            {
                var result = action.HandleMouseDown(e);

                if (result == ActionResult.StopHere)
                    break;
            }
        }

        private void Action_Finished(object sender, EventArgs e)
        {
            UIActionBase action = sender as UIActionBase;

            if (action != null && actionStack.Contains(action))
            {
                actionStack.Remove(action);
                action.OnRemoved();
            }
        }
    }
}
