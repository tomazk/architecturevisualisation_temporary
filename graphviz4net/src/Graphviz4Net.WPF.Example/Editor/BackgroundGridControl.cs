﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Graphviz4Net.WPF.Example.Editor
{
    /// <summary>
    /// Provides a background grid for the designer
    /// </summary>
    public class BackgroundGridControl : Control
    {
        private Pen gridPen;

        static BackgroundGridControl()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(BackgroundGridControl), new FrameworkPropertyMetadata(typeof(BackgroundGridControl)));
        }

        public BackgroundGridControl()
        {
            gridPen = new Pen(Brushes.Lavender, 1);
        }

        private Transform transform = Transform.Identity;
        public Transform Transform
        {
            get { return transform; }
            set
            {
                transform = value;
                this.Dispatcher.Invoke(() => InvalidateVisual());
            }
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            double d = 20;

            // Calculate two points in transformed space
            Point p1 = transform.Transform(new Point());
            Point p2 = transform.Transform(new Point(d, 0));
            d = p2.X - p1.X;

            // Limit pixel density
            while (d < 20)
            {
                d *= 2;
            }
            while (d > 40)
            {
                d /= 2;
            }

            // Calculate start position (top left corder)
            double x = p1.X % d;
            double y = p1.Y % d;
            if (x < 0)
                x += d;
            if (y < 0)
                y += d;

            // Draw lines
            while (x < RenderSize.Width)
            {
                drawingContext.DrawLine(gridPen, new Point(x, 0), new Point(x, RenderSize.Height));
                x += d;
            }
            while (y < RenderSize.Height)
            {
                drawingContext.DrawLine(gridPen, new Point(0, y), new Point(RenderSize.Width, y));
                y += d;
            }
        }
    }
}
