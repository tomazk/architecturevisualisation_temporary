﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Graphviz4Net.WPF.Example.Editor
{
    public class EditorLayer
    {
        internal Canvas Canvas { get; }

        public EditorLayer()
        {
            Canvas = new Canvas();
        }

        public void AddElement(UIElement uiElement, Point position)
        {
            Canvas.Children.Add(uiElement);

            uiElement.SetValue(Canvas.LeftProperty, position.X);
            uiElement.SetValue(Canvas.TopProperty, position.Y);
        }
        public void RemoveComponent(UIElement uiElement)
        {
            Canvas.Children.Remove(uiElement);
        }
        public IEnumerable<UIElement> Children => Canvas.Children.OfType<UIElement>();

        internal void SetTransform(Transform transform)
        {
            Canvas.RenderTransform = transform;
        }
    }
}
