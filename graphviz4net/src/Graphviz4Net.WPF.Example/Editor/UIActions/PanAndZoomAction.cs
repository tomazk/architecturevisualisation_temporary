﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Graphviz4Net.WPF.Example.Editor.UIActions
{
    public class PanAndZoomAction : UIActionBase
    {
        private EditorView editorView;
        private Transform panStartTransform = Transform.Identity;
        private Point startPoint;
        private bool isPanning = false;

        public PanAndZoomAction(EditorView editorView)
            : base(editorView)
        {
            this.editorView = editorView;
        }

        public override void Cancel()
        {
        }

        public override ActionResult HandleKeyDown(KeyEventArgs e)
        {
            return ActionResult.PassOn;
        }

        public override ActionResult HandleMouseDown(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle && e.MiddleButton == MouseButtonState.Pressed &&
                e.LeftButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released)
            {
                Point mousePosition = Mouse.GetPosition(editorView);
                EditorView.CaptureMouse();
                startPoint = mousePosition;
                panStartTransform = editorView.Tranform;
                isPanning = true;

                return ActionResult.StopHere;
            }
            else
                return ActionResult.PassOn;
        }

        public override ActionResult HandleMouseMove(MouseEventArgs e)
        {
            if (isPanning)
            {
                Point p = Mouse.GetPosition(editorView);
                Transform transform = panStartTransform;

                Matrix m = transform.Value;
                m.Translate(p.X - startPoint.X, p.Y - startPoint.Y);
                transform = new MatrixTransform(m);
                editorView.Tranform = transform;

                return ActionResult.StopHere;
            }
            else
                return ActionResult.PassOn;
        }

        public override ActionResult HandleMouseUp(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle && e.MiddleButton == MouseButtonState.Released)
            {
                isPanning = false;
                editorView.ReleaseMouseCapture();
                return ActionResult.StopHere;
            }
            else
                return ActionResult.PassOn;
        }

        public override ActionResult HandleMouseWheel(MouseWheelEventArgs e)
        {
            Point mousePosition = Mouse.GetPosition(editorView);
            double zoomFactor = 1.1;

            double scale = (e.Delta < 0) ? (1 / zoomFactor) : (zoomFactor);

            Matrix newMatrix = new Matrix();
            newMatrix.Append(editorView.Tranform.Value);
            newMatrix.ScaleAt(scale, scale, mousePosition.X, mousePosition.Y);

            editorView.Tranform = new MatrixTransform(newMatrix);

            return ActionResult.StopHere;
        }
    }
}
