﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Graphviz4Net.WPF.Example.Editor.UIActions
{
    public abstract class UIActionBase
    {
        public UIActionBase(EditorView editorView)
        {
            this.EditorView = editorView;
        }

        public EditorView EditorView { get; }

        public abstract ActionResult HandleMouseMove(MouseEventArgs e);
        public abstract ActionResult HandleMouseDown(MouseButtonEventArgs e);
        public abstract ActionResult HandleMouseUp(MouseButtonEventArgs e);
        public abstract ActionResult HandleMouseWheel(MouseWheelEventArgs e);
        public abstract ActionResult HandleKeyDown(KeyEventArgs e);
        public abstract void Cancel();
        public virtual void OnAdded()
        {
        }
        public virtual void OnRemoved()
        {
        }

        protected void OnFinished()
        {
            Finished?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler Finished;
    }

    public enum ActionResult
    {
        /// <summary>
        /// Stops the event processing at this level
        /// </summary>
        StopHere,
        /// <summary>
        /// Passes the event through to the next level
        /// </summary>
        PassOn
    }
}
