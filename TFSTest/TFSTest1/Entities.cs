﻿using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TFSTest1
{
    public class TeamProject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }


        public Project Project { get; set; }
    }

    public class TeamProjectIteration
    {
        public string IterationPath { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? FinishDate { get; set; }
    }

    public class TeamProjectWorkItem
    {
        public int Id { get; set; }
        public string TitleEN { get; set; }
        public string TitleDE { get; set; }
        public bool IsPublic { get; set; }
        public bool IsHighlight { get; set; }
        public bool IsHidden { get; set; }
        public string Title { get; set; }
        public string Reason { get; set; }
        public string State { get; set; }
        public string SystemInfo { get; set; }
    }
}
