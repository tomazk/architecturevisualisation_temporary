﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.ProcessConfiguration.Client;
using Microsoft.TeamFoundation.Server;
using Microsoft.TeamFoundation.VersionControl.Client;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TFSTest1
{
    public class TFS
    {
        private string _tfsPath;
        private TfsTeamProjectCollection _collection;
        private WorkItemStore _workItemStore;
        private TeamSettingsConfigurationService _teamSettingsConfigurationService;
        private ICommonStructureService4 _commonStructureService;
        private VersionControlServer _versionControlServer;

        // lock used to sync the loading of the team projects in the background thread
        private ManualResetEvent _loadFinishedSignal = new ManualResetEvent(false);
        private IEnumerable<TeamProject> _teamProjects;

        public TFS(string tfsPath)
        {
            _tfsPath = tfsPath;

            _collection = TfsTeamProjectCollectionFactory.GetTeamProjectCollection(new Uri(_tfsPath));
            //_server = new TfsTeamProjectCollection(uri, new TfsClientCredentials(true));
            _workItemStore = _collection.GetService<WorkItemStore>();
            _versionControlServer = (VersionControlServer)_collection.GetService(typeof(VersionControlServer));
        }

        public IEnumerable<Workspace> GetWorkspaces()
        {
            Workstation.Current.EnsureUpdateWorkspaceInfoCache(_versionControlServer, Environment.UserName);
            return _versionControlServer.QueryWorkspaces(null, Environment.MachineName, Environment.UserName);
        }
        public Workspace GetCurrentWorkspace()
        {
            // TODO: what happens if there are many workspaces?
            return _versionControlServer.GetWorkspace(Environment.MachineName, Environment.UserName);
        }

        public string MapLocalToServerFilename(string localFilename)
        {
            var workspace = GetCurrentWorkspace();
            return workspace.TryGetServerItemForLocalItem(localFilename);
        }

        public IEnumerable<Changeset> GetLocalFileChangesets(string localFilename)
        {
            var workspace = GetCurrentWorkspace();
            var serverFilename = MapLocalToServerFilename(localFilename);
            var changeSets = _versionControlServer.QueryHistory(serverFilename, RecursionType.Full);

            return changeSets;
        }

        public IEnumerable<TeamProject> TeamProjects
        {
            get
            {
                _loadFinishedSignal.WaitOne();

                return _teamProjects;
            }
        }

        public string LocalToRemoteFilename(string localFilename)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TeamProjectIteration> GetProjectIterations(int teamProjectId)
        {
            TeamProject project = TeamProjects.SingleOrDefault(x => x.Id == teamProjectId);

            if (project == null)
            {
                // Bug: sometimes project is null, but works after a break:
                Thread.Sleep(500);

                project = TeamProjects.SingleOrDefault(x => x.Id == teamProjectId);

                if (project == null)
                    throw new Exception("Project doesn't exit");
            }

            var tmpProject = _commonStructureService.GetProjectFromName(project.Name);

            Debug.WriteLine(Environment.UserName);

            // Get project iterations
            IEnumerable<TeamConfiguration> configs = _teamSettingsConfigurationService.GetTeamConfigurationsForUser(new[] { tmpProject.Uri });

            if (!configs.Any())
                throw new Exception("Unable to find configuration for selected team project.");

            //TeamConfiguration teamConfigOld = configs.FirstOrDefault(x => x.ProjectUri == project.Path);
            TeamConfiguration teamConfig = configs.FirstOrDefault(x => x.IsDefaultTeam);

            var iterations = BuildIterationTree(teamConfig);
            return iterations;
        }

        /*public IEnumerable<TeamProjectWorkItem> GetProjectWorkItemsOld(int teamProjectId, TeamProjectIteration iteration)
        {
            TeamProject project = TeamProjects.SingleOrDefault(x => x.Id == teamProjectId);

            if (project == null)
                throw new Exception("Project doesn't exit");

            var tmpList = _workItemStore.Query(
                    "SELECT [System.Id], [System.WorkItemType]," +
                    " [System.State], [System.AssignedTo], [System.Title] " +
                    " FROM WorkItems " +
                    $" WHERE [System.TeamProject] = '{project.Name}'" +
                    $" AND [System.IterationPath] = '{iteration.IterationPath}'" +
                    " ORDER BY [System.WorkItemType], [System.Id]");

            List<TeamProjectWorkItem> list = new List<TeamProjectWorkItem>();

            foreach (var item in tmpList)
            {
                WorkItem workItem = item as WorkItem;

                string systemInfo = null;

                // Find "Systeminfo" field where EN and DE work item title translations are stored
                foreach (var fieldItem in workItem.Fields)
                {
                    Microsoft.TeamFoundation.WorkItemTracking.Client.Field field = (Microsoft.TeamFoundation.WorkItemTracking.Client.Field)fieldItem;
                    if (field.Name.Equals("Systeminfo", StringComparison.OrdinalIgnoreCase))
                        systemInfo = field.Value.ToString();
                }

                list.Add(new TeamProjectWorkItem
                {
                    Id = workItem.Id,
                    Title = workItem.Title,
                    State = workItem.State,
                    Reason = workItem.Reason,
                    SystemInfo = systemInfo
                });
            }

            return list;
        }*/

        /*/// <summary>
        /// 
        /// </summary>
        /// <param name="teamProjectId"></param>
        /// <param name="includedIterationPaths">all iterations path that should be included. if no path is given all iterations are included</param>
        /// <param name="changes"></param>
        /// <param name="fixedbugs"></param>
        /// <param name="notFixedbugs"></param>
        public void GetProjectWorkItems(int teamProjectId, List<string> includedIterationPaths, List<string> tags, out List<TeamProjectWorkItem> changes, out List<TeamProjectWorkItem> fixedbugs, out List<TeamProjectWorkItem> notFixedbugs)
        {
            changes = new List<TeamProjectWorkItem>();
            fixedbugs = new List<TeamProjectWorkItem>();
            notFixedbugs = new List<TeamProjectWorkItem>();

            TeamProject project = TeamProjects.SingleOrDefault(x => x.Id == teamProjectId);
            WorkItemCollection foundUserStories, fixedTfsBugs, notFixedTfsBugs;

            bool isPublic = false;
            bool highlight = false;
            bool isHidden = false;
            string description = string.Empty;
            string releaseTitleEN = string.Empty;
            string releaseTitleDE = string.Empty;

            if (project == null)
                throw new Exception("Project doesn't exit");

            //generate iteration filter 
            string iterationFilter = "";
            if (includedIterationPaths != null && includedIterationPaths.Count > 0)
            {
                foreach (string iteration in includedIterationPaths)
                {
                    if (iterationFilter != "")
                    {
                        iterationFilter += " || ";
                    }

                    iterationFilter += $" [Iterationspfad] Under '{iteration}'";
                }
            }
            else
            {
                throw new Exception("no iterations found");
            }

            //generate tags filter
            string tagsFilter = null;
            if (tags != null && tags.Count > 0)
            {
                foreach (string tag in tags)
                {
                    if (string.IsNullOrWhiteSpace(tag))
                        continue;

                    if (tagsFilter != null)
                    {
                        tagsFilter += " || ";
                    }

                    tagsFilter += $" [Tags] Contains '{tag}'";
                }
            }

            // User Stories
            string query = $"Select * From WorkItems Where ([Arbeitsaufgabentyp] = 'User Story' || [Arbeitsaufgabentyp] = 'Product Backlog Item')" +
                $" && [Teamprojekt] = '{project.Name}'" +
                $" && ([Zustand] = 'Geschlossen' || [Zustand] = 'Closed' || [Zustand] = 'Fertig' || [Zustand] = 'Resolved' || [Zustand] = 'Gelöst')" +
                $" && ([Grund für Lösung] = 'Fixed' || [Grund für Lösung] = 'Korrigiert' || [Grund für Lösung] = '')" +
                $" && ({iterationFilter})" +
                (string.IsNullOrWhiteSpace(tagsFilter) ? "" : $" && ({tagsFilter})");

            foundUserStories = _workItemStore.Query(query);

            foreach (WorkItem userStory in foundUserStories)
            {

                // get titles and description
                GetReleaseTexts(userStory, "", out isPublic, out releaseTitleEN, out releaseTitleDE, out description, out highlight, out isHidden);

                changes.Add(new TeamProjectWorkItem
                {
                    Id = userStory.Id,
                    TitleEN = releaseTitleEN,
                    TitleDE = releaseTitleDE,
                    IsPublic = isPublic,
                    IsHighlight = highlight,
                    IsHidden = isHidden,
                    Title = description,
                    State = userStory.State,
                    Reason = userStory.Reason,
                    SystemInfo = ""
                });

            }

            // Fixed Bugs
            string queryFixedBugs = $"Select * From WorkItems Where ([Arbeitsaufgabentyp] = 'Bug' || [Arbeitsaufgabentyp] = 'Fehler')" +
                $" && [Teamprojekt] = '{project.Name}'" +
                $" && ([Zustand] = 'Geschlossen' || [Zustand] = 'Closed' || [Zustand] = 'Resolved' || [Zustand] = 'Gelöst') " +
                $"&& ([Grund für Lösung] = 'Fixed' || [Grund für Lösung] = 'Korrigiert' || [Grund für Lösung] = '')" +
                $" && ({iterationFilter})" +
                (string.IsNullOrWhiteSpace(tagsFilter) ? "" : $" && ({tagsFilter})");


            fixedTfsBugs = _workItemStore.Query(queryFixedBugs);
            foreach (WorkItem fixedBug in fixedTfsBugs)
            {

                // get titles and description
                GetReleaseTexts(fixedBug, "Systeminfo", out isPublic, out releaseTitleEN, out releaseTitleDE, out description, out highlight, out isHidden);

                fixedbugs.Add(new TeamProjectWorkItem
                {
                    Id = fixedBug.Id,
                    TitleEN = releaseTitleEN,
                    TitleDE = releaseTitleDE,
                    IsPublic = isPublic,
                    IsHighlight = highlight,
                    IsHidden = isHidden,
                    Title = description,
                    State = fixedBug.State,
                    Reason = fixedBug.Reason,
                    SystemInfo = ""
                });

            }

            // Not fixed bugs
            string queryNotFixedBugs = $"Select * From WorkItems Where ([Arbeitsaufgabentyp] = 'Bug' || [Arbeitsaufgabentyp] = 'Fehler')" +
                $" && [Teamprojekt] = '{project.Name}'" +
                $" && ([Zustand] = 'Geschlossen' || [Zustand] = 'Closed')" +
                $" && ([Grund für Lösung] != 'Fixed' && [Grund für Lösung] != 'Korrigiert' && [Grund für Lösung] != '')" +
                $" && ({iterationFilter})" +
                (string.IsNullOrWhiteSpace(tagsFilter) ? "" : $" && ({tagsFilter})");

            notFixedTfsBugs = _workItemStore.Query(queryNotFixedBugs);
            foreach (WorkItem notFixedBug in notFixedTfsBugs)
            {

                // get titles and description
                GetReleaseTexts(notFixedBug, "Systeminfo", out isPublic, out releaseTitleEN, out releaseTitleDE, out description, out highlight, out isHidden);

                fixedbugs.Add(new TeamProjectWorkItem
                {
                    Id = notFixedBug.Id,
                    TitleEN = releaseTitleEN,
                    TitleDE = releaseTitleDE,
                    IsPublic = isPublic,
                    IsHighlight = highlight,
                    IsHidden = isHidden,
                    Title = description,
                    State = notFixedBug.State,
                    Reason = notFixedBug.Reason,
                    SystemInfo = ""
                });

            }
        }*/

        private IList<TeamProjectIteration> BuildIterationTree(TeamConfiguration teamConfiguration)
        {
            string[] paths = teamConfiguration.TeamSettings.IterationPaths;

            var result = new List<TeamProjectIteration>();

            foreach (string nodePath in paths.OrderBy(x => x))
            {
                var projectNameIndex = nodePath.IndexOf("\\", 2);
                var fullPath = nodePath.Insert(projectNameIndex, "\\Iteration");
                var nodeInfo = _commonStructureService.GetNodeFromPath(fullPath);
                var name = nodeInfo.Name;
                var startDate = nodeInfo.StartDate;
                var endDate = nodeInfo.FinishDate;

                result.Add(new TeamProjectIteration
                {
                    IterationPath = nodePath,
                    StartDate = startDate,
                    FinishDate = endDate,
                });
            }
            return result;
        }

        private object loadProjectsLock = new object();
        public void LoadProjects()
        {
            lock (loadProjectsLock)
            {
                _teamProjects = _workItemStore.Projects
                    .Cast<Project>()
                    .Select(x =>
                    {
                        return new TeamProject
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Path = _tfsPath + "/" + x.Name,
                            Project = x
                        };
                    });
                _teamSettingsConfigurationService = _collection.GetService<TeamSettingsConfigurationService>();
                _commonStructureService = _collection.GetService<ICommonStructureService4>();

                _loadFinishedSignal.Set();
            }
        }
    }
}
